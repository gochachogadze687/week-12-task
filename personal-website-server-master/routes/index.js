const express = require('express');
const router = express.Router();
const FileStorage = require('../services/FileStorage');

/* POST /subscribe */
router.post('/subscribe', async function(req, res) {
    try {
        if (!req.body || !req.body.email) {
            return res.status(400).json({ error: "Wrong payload" });
        }

        if (req.body.email === 'forbidden@gmail.com') {
            return res.status(422).json({ error: "Email is already in use" });
        }

        const data = { email: req.body.email };
        await FileStorage.writeFile('user.json', data);
        await res.json({ success: true })
    } catch (e) {
        console.log(e);
        res.status(500).send('Internal error');
    }
});

/* GET /unsubscribe */
router.post('/unsubscribe', async function(req, res) {
    try {
        await FileStorage.deleteFile('user.json');
        await FileStorage.writeFile('user-analytics.json', []);
        await FileStorage.writeFile('performance-analytics.json', []);
        await res.json({ success: true })
    } catch (e) {
        console.log(e);
        res.status(500).send('Internal error');
    }
});


/* GET /community */
router.get('/community', async function(req, res) {
    try {
        // Sample community data (replace this with your actual data from the server)
        const communityData = [
            {
                id: "2f1b6bf3-f23c-47e4-88f2-e4ce89409376",
                avatar: "http://localhost:3000/avatars/avatar1.png",
                firstName: "Mary",
                lastName: "Smith",
                position: "Lead Designer at Company Name",
            },
            {
                id: "1157fea1-8b72-4a9e-b253-c65fa1556e26",
                avatar: "http://localhost:3000/avatars/avatar2.png",
                firstName: "Bill",
                lastName: "Filler",
                position: "Lead Engineer at Company Name",
            },
            {
                id: "b96ac290-543c-4403-80fe-0c2d44e84ea9",
                avatar: "http://localhost:3000/avatars/avatar3.png",
                firstName: "Tim",
                lastName: "Gates",
                position: "CEO at Company Name",
            },
        ];

        res.json(communityData);
    } catch (e) {
        console.log(e);
        res.status(500).send('Internal error');
    }



});



      module.exports = router;