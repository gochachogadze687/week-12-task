const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

module.exports = (env, argv) => {
    const isProduction = argv.mode === 'production';

    return {
        entry: './personal-website-master/src/main.js',
        mode: isProduction ? 'production' : 'development',
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist'),
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: 'babel-loader',
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.(png|jpg|jpeg|gif)$/i,
                    type: 'asset/resource',
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './personal-website-master/src/index.html',
            }),
            new CopyWebpackPlugin({
                patterns: [
                    { from: './personal-website-master/src/assets', to: 'assets' },
                ],
            }),
            new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
            new webpack.DefinePlugin({
                'process.env.API_BASE_URL': JSON.stringify(isProduction ? '/api' : '/'),
              }),
        ],
        optimization: {
            minimizer: isProduction
                ? [
                    new TerserWebpackPlugin({
                        terserOptions: {
                            compress: {
                                drop_console: true,
                            },
                        },
                    }),
                ]
                : [],
        },
        devServer: {
            static: {
                directory: path.resolve(__dirname, 'public'),
            },
            port: 8080,
            proxy: {
                '/api': 'http://localhost:3000',
              },
        },
    };
};
