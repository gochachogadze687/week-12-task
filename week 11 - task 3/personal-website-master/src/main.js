import './styles/style.css';

const emailInput = document.getElementById('email');
const joinForm = document.getElementById('joinForm');
const unsubscribeContainer = document.getElementById('unsubscribeContainer');

function saveEmailToLocalStorage() {
  const emailValue = emailInput.value;
  localStorage.setItem('subscriptionEmail', emailValue);
}

emailInput.addEventListener('input', saveEmailToLocalStorage);

function populateEmailFromLocalStorage() {
  const storedEmail = localStorage.getItem('subscriptionEmail');
  if (storedEmail) {
    emailInput.value = storedEmail;
  }
}

populateEmailFromLocalStorage();

function hideEmailInputAndShowButton() {
  const isValid = emailInput.checkValidity();
  if (isValid) {
    emailInput.style.display = 'none';
    unsubscribeContainer.innerHTML = `
      <button type="button" id="unsubscribeButton">Unsubscribe</button>
    `;
    unsubscribeButton.addEventListener('click', handleUnsubscribe);
  }
}

function handleUnsubscribe() {
  emailInput.style.display = 'block';
  unsubscribeContainer.innerHTML = '';
  localStorage.removeItem('subscriptionEmail');
}

joinForm.addEventListener('submit', (e) => {
  e.preventDefault();
  hideEmailInputAndShowButton();

  const email = emailInput.value;
  fetch('http://localhost:3002/api/subscribe', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email }),
  })
    .then((response) => response.json())
    .then((data) => console.log(data))
    .catch((error) => console.error(error));

});
